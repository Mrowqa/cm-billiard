#ifndef OBJECT_H
#define OBJECT_H

#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>
#include <limits>
#include "../Render/Render.h"
#include "../CMBilliardConfig.h"
#include <cassert>
#include <memory>

#define PIXELSTOMETERS 50
#define METERSTOPIXLES (1.f/PIXELSTOMETERS)

#define DEGTORAD 0.0174532925199432957f
#define RADTODEG 57.295779513082320876f

#define WRONG_ID ((uint)-1)

typedef unsigned int uint;
const float fqNaN = std::numeric_limits<float>::quiet_NaN();
/*
 * TODO:
 *  delete SpriteID -- add draw method, sf::Texture/sf::Sprite members, ...\
 */

class Object
{
private:
	std::string textureFileName;

protected:
	b2Body* body;
	std::shared_ptr<sf::Texture> texture;
	//sf::Vector2f scale; // used for sprites
	//sf::Vector2f pos; // if body not connected use this?
	//float rotation;
	Render* render;

	int state; // only two states... 'if' is easier than machine state :)

	bool loadTexture()			{if(!render || textureFileName.empty()) return false; texture=render->loadTexture(textureFileName); return static_cast<bool>(texture);}
	void freeTexture()			{if(!render || textureFileName.empty() || texture.use_count()>2 || texture || !releaseResourcesWhenNotNeeded) return; render->releaseTexture(textureFileName); texture.reset();}

public:
	bool setPos(float x, float y, float PixelsPerMeter=PIXELSTOMETERS, sf::Vector2f offset=sf::Vector2f(0,0))		{assert(body); body->SetTransform(b2Vec2((x+offset.x)*PixelsPerMeter,-((y+offset.y)*PixelsPerMeter)),body->GetAngle()); return true;}
	sf::Vector2f getPos(float PixelsPerMeter=PIXELSTOMETERS, sf::Vector2f offset=sf::Vector2f(0,0)) const			{assert(body); return sf::Vector2f(body->GetPosition().x/PixelsPerMeter-offset.x,-(body->GetPosition().y/PixelsPerMeter-offset.y));}
	bool setAngle(float angle)									{assert(body); body->SetTransform(body->GetPosition(),angle*DEGTORAD); return true;}
	float getAngle(float angle)	const							{assert(body); return body->GetAngle()*RADTODEG;} // return 0 instead assert?
	b2Body* connectBody(b2Body* newBody)						{b2Body* old=body; body=newBody; onConnectBody(old,newBody); return old;} // not better make these methods protected and declare friend?
	b2Body* disconnectBody()									{return connectBody(nullptr);}
	virtual void onConnectBody(b2Body* old, b2Body* newBody)	{}
	b2Body* getBody() const										{return body;}
	int changeState(int newState)								{int old=state; state=newState; onChangeState(old,newState); return old;}
	int getState() const										{return state;}
	virtual void onChangeState(int oldState, int newState)		{}
	Render* changeRender(Render* newRender)						{Render* old=render; freeTexture(); render=newRender; loadTexture(); return old;}
	std::string changeTexture(std::string pathToNewTexture)		{std::string old=textureFileName; freeTexture(); textureFileName=pathToNewTexture; loadTexture(); return old;}
	bool draw()													{if(!texture || !render) return false; onDraw(); return true;} // check body?
	virtual void onDraw()=0;

	static bool releaseResourcesWhenNotNeeded;

	Object(Render* _render=nullptr, std::string _textureFileName="") : textureFileName(_textureFileName), body(nullptr), render(_render) {loadTexture();}
	virtual ~Object(void) {freeTexture();}
};

#endif // OBJECT_H
