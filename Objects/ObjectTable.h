#ifndef OBJECTTABLE_H
#define OBJECTTABLE_H

#include "ObjectBall.h"
#include "../Physics/BilliardPhysics.h"
#include <vector>

class ObjectTable :
	public Object
{
protected:
	BilliardPhysics bPhysics; // inherits instead?
	std::vector<ObjectBall> balls;

public:
	ObjectTable(Render* render=nullptr) : Object(render,CM_BILLIARD_DATA_DIR"textures/tables.png"), balls(16), playerOne(TwilightSparkle), playerTwo(RainbowDash) {}
	virtual ~ObjectTable(void) {}

	bool create(BilliardPhysics::PoolTable tableType=BilliardPhysics::Rectangle, BilliardPhysics::GameType gameType=BilliardPhysics::EightBall);
	bool step() const																				{return bPhysics.step();}
	bool hitBall(float force, float angle=0.f, int ballNumber=0) const								{return bPhysics.hitBall(force,angle,ballNumber);}
	bool testNewPos(float x, float y, int ballNumber=0, float PixelsPerMeter=PIXELSTOMETERS) const	{return bPhysics.testNewPos(x*PixelsPerMeter,y*PixelsPerMeter,ballNumber);}
	bool moveBall(float x, float y, int ballNumber=0, float PixelsPerMeter=PIXELSTOMETERS) const	{return bPhysics.moveBall(x*PixelsPerMeter,y*PixelsPerMeter,ballNumber);}
	bool pushBall(int ballNumber=0, float step=0.1f) const											{return bPhysics.pushBall(ballNumber,step);}
	bool isGameExist() const																		{return bPhysics.isGameExist();}
	const std::vector<int>& getLastStuckedBalls() const												{return bPhysics.getLastStuckedBalls();}
	int getState() const																			{return bPhysics.getState();}
	virtual void onDraw();

	enum PlayerType { RainbowDash, Fluttershy, TwilightSparkle, Rarity, PinkiePie, Applejack } playerOne, playerTwo;
	// game sets sprites id
};

#endif // OBJECTTABLE_H