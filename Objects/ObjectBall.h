#ifndef OBJECTBALL_H
#define OBJECTBALL_H

#include "Object.h"
//#include "../Physics/BilliardPhysics.h"

class ObjectBall :
	public Object
{
public:
	enum State { Normal, InPocket, Moving };
	int playerOne, playerTwo;
	int gameType; // in .cpp included ObjectTable with enum :)

	virtual void onChangeState(int oldState, int newState)			{assert(body); body->GetFixtureList()->SetSensor(newState!=Normal); if(newState==InPocket) body->SetLinearVelocity(b2Vec2(0,0));}
	virtual void onConnectBody(b2Body* oldBody, b2Body* newBody)	{state=(newBody? (newBody->GetFixtureList()->IsSensor() ? InPocket : Normal) : WRONG_ID);}

	// correct getting pos -- box2d/sfml object centers

	ObjectBall(Render* render=nullptr) : Object(render,CM_BILLIARD_DATA_DIR"textures/balls.png"), ballNumber(WRONG_ID) {}
	virtual ~ObjectBall(void) {}
	virtual void onDraw();

	int ballNumber;
};


#endif // OBJECTBALL_H
