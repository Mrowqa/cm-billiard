#include "ObjectTable.h"


bool ObjectTable::create(BilliardPhysics::PoolTable tableType, BilliardPhysics::GameType gameType)
{
	if(!bPhysics.createNewGame(tableType,gameType)) return false;

	int ballsCount=0;
	switch(gameType)
	{
	case BilliardPhysics::EightBall:
		ballsCount=16;
		break;
	}
	balls.resize(ballsCount);

	for(auto& it : balls)
	{
		it.gameType = gameType;
		it.playerOne = playerOne;
		it.playerTwo = playerTwo;
	}

	if(!bPhysics.connect(balls)) return false;
}

void ObjectTable::onDraw()
{
	sf::RenderWindow& wnd = render->window;
	sf::Vector2f tableCenter(wnd.getSize().x/2.f, wnd.getSize().y/2.f+100); // first height 200px is header
	
	sf::Sprite tableSprite(*texture);
	// texture crop? (when more tables?)
	tableSprite.setOrigin(tableSprite.getLocalBounds().width/2, tableSprite.getLocalBounds().height/2);
	wnd.draw(tableSprite,sf::Transform().translate(tableCenter));

	for(auto& it : balls)
		it.draw();
}