#include "Render.h"
#include <stdexcept>

std::shared_ptr<sf::Texture> Render::loadTexture(std::string path)
{
	path=correctPath(path);
	try
	{
		return textureMap.at(path);
	}
	catch(std::out_of_range&)
	{
		std::shared_ptr<sf::Texture> texture=std::make_shared<sf::Texture>();
		if(!texture->loadFromFile(path))
		{
			texture.reset();
			return texture;
		}

		textureMap[path]=texture;
		return texture;
	}
}

void Render::releaseTexture(std::string path)
{
	path=correctPath(path);
	textureMap.erase(textureMap.find(path));
}

void Render::releaseAllResources()
{
	textureMap.clear();
}

void Render::releaseUnusedResources()
{
	for(auto it=textureMap.begin(); it!=textureMap.end(); it++)
		if(it->second.unique()) textureMap.erase(it);
}

std::string Render::correctPath(std::string path)
{
	std::size_t pos=0,pos2=0;
	while((pos=path.find('\\',pos+1))!=std::string::npos)
	{
		path[pos]='/';
	}
	while((pos=path.find("//",pos+1))!=std::string::npos)
	{
		path.replace(pos,2,"/");
	}
	while((pos=path.find("/../",pos+1))!=std::string::npos)
	{
		if(pos==0) continue;
		pos2=path.rfind('/',pos-1);
		if(path.substr(pos2,4)!="/../") path.replace(pos2,3,"");
	}
	return path;
}
