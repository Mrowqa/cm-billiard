#ifndef RENDER_H
#define RENDER_H
 
#include <SFML/Graphics.hpp>
#include <map>
#include <string>
#include <memory>

/*
 * I thought about custom render, but for long time, it doesn't make sense. 
 * 
 * Add shaders?
 */
class Render
{
public:
	sf::RenderWindow window;
	sf::Vector2u defaultResolution; // using for texture scaling

	std::shared_ptr<sf::Texture> loadTexture(std::string path);
	void releaseTexture(std::string path);
	void releaseAllResources();
	void releaseUnusedResources();
	
	sf::Vector2f getResolutionFactors() const {return sf::Vector2f(static_cast<float>(defaultResolution.x)/window.getSize().x,static_cast<float>(defaultResolution.y)/window.getSize().y);} // how many times actual resolution is greater than default

	Render() {}
	~Render() {releaseAllResources();}

private:
	std::map<std::string,std::shared_ptr<sf::Texture>> textureMap;

	std::string correctPath(std::string path);
};
     
#endif // RENDER_H
