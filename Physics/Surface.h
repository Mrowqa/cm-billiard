#ifndef SURFACE_H
#define SURFACE_H

#include <vector>

class SurfaceInequality
{
public:
	float a,b,c; // Ax+By+C<op>0
	enum Inequality {Greater, Less, GreaterOrEqual, LessOrEqual} inequality;

	SurfaceInequality(float A=.0f, float B=.0f, float C=.0f, Inequality ineq=GreaterOrEqual) : a(A), b(B), c(C), inequality(ineq) {}
	bool isPointLying(float x, float y) const
	{
		switch(inequality)
		{
		case Greater:
			return a*x+b*y+c > 0.f;
			break;
			
		case GreaterOrEqual:
			return a*x+b*y+c >= 0.f;
			break;
			
		case Less:
			return a*x+b*y+c < 0.f;
			break;
			
		case LessOrEqual:
			return a*x+b*y+c <= 0.f;
			break;
		}

		return a*x+b*y+c >= 0.f;
	}
};

/**
	Convex shape
*/
class Surface
{
public:
	std::vector<SurfaceInequality> surface;

	bool testPoint(float x, float y) const
	{
		for(const SurfaceInequality& sIneq : surface)
			if(sIneq.isPointLying(x,y)==false) return false;

		return true;
	}
	void clearInequalites() { surface.clear(); }
	void addInequalite(float A=.0f, float B=.0f, float C=.0f, SurfaceInequality::Inequality ineq=SurfaceInequality::GreaterOrEqual)
	{
		surface.push_back(SurfaceInequality(A,B,C,ineq));
	}

	Surface(void) {}
	~Surface(void) {}
};

#endif // SURFACE_H