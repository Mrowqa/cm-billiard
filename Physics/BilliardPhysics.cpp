#define toDegrees(angle) ((angle)*180.f/b2_pi)
#define toRadians(angle) ((angle)*b2_pi/180.f)

#define DEGTORAD 0.0174532925199432957f
#define RADTODEG 57.295779513082320876f

#define SQRT2 1.4142135623730950488016887242097f
#define SQRT3 1.7320508075688772935274463415059f

#include <Box2D/Box2D.h>
#include <set>
#include "BilliardPhysics.h"
using namespace std;

set<b2Body*> destroyQueue; // set - we have certain that won't be any multiple bodys

void PocketContactListener::BeginContact(b2Contact* contact)
{
	b2Fixture* fixtureA = contact->GetFixtureA();
	b2Fixture* fixtureB = contact->GetFixtureB();
  
	//make sure only one of the fixtures was a sensor
	bool sensorA = fixtureA->IsSensor();
	bool sensorB = fixtureB->IsSensor();
	if ( ! (sensorA ^ sensorB) )
		return;

	destroyQueue.insert(sensorA ? fixtureB->GetBody() : fixtureA->GetBody());
}

inline int32 sgn(float32 value)
{
	return value<0?-1 : value>0?1 : 0;
}


BilliardPhysics::BilliardPhysics(PoolTable tableType /*= Rectangle*/, GameType gType /*= EightBall*/) : actualTable(tableType), actualGame(gType), connected(false), world(nullptr), sensors(nullptr)
{
	createNewGame(tableType,gType);
}

bool BilliardPhysics::createNewGame(PoolTable tableType /*= Rectangle*/, GameType gType /*= EightBall*/)
{
	deleteGame(); // delete actual game

	connected = false;
	actualGame = gType;
	actualTable = tableType;

	b2Vec2 gravity(0,0); // without gravition
	world = new(std::nothrow) b2World(gravity);
	if(!world) return false;
	world->SetContactListener(&myPCListener);

	ballRadius = 5.f/16.f; //5.f / 20.f;
	b2Vec2 whiteBallPos, firstBallPos;


	//make a static floor to drop things on
	b2BodyDef tableDef;
	tableDef.type = b2_staticBody;
	tableDef.position.Set(0, 0); //middle, bottom
	b2Body* table = world->CreateBody(&tableDef);
	sensors = world->CreateBody(&tableDef);
	
	//float ballRadius = 0.f;
	
	switch(tableType)
	{
	case Rectangle:
		{
			b2Vec2 tableSize(10,5); // box 20x10
			//float32 ballRadius = tableSize.y / 20.f;
			float32 pocketFactor = 2.25f;
			
			whiteBallPos.Set(-tableSize.x*0.50f,0);
			firstBallPos.Set(tableSize.x*0.50f,0);

			actualSurface.clearInequalites();
			actualSurface.addInequalite(-tableSize.x+ballRadius,0,0,SurfaceInequality::Greater); // left bound
			actualSurface.addInequalite(0,tableSize.y-ballRadius,0,SurfaceInequality::Less); // top bound
			actualSurface.addInequalite(tableSize.x-ballRadius,0,0,SurfaceInequality::Less); // right bound
			actualSurface.addInequalite(0,-tableSize.y+ballRadius,0,SurfaceInequality::Greater); // bottom bound
			
			b2Vec2 vertices[] = { b2Vec2(-tableSize.x,tableSize.y-ballRadius*pocketFactor*SQRT2), b2Vec2(-tableSize.x-ballRadius*pocketFactor*SQRT2,tableSize.y), b2Vec2(-tableSize.x,tableSize.y+ballRadius*pocketFactor*SQRT2), b2Vec2(-tableSize.x+ballRadius*pocketFactor*SQRT2,tableSize.y), // up left corner
				b2Vec2(-ballRadius*pocketFactor,tableSize.y), b2Vec2(-ballRadius,tableSize.y+ballRadius*2*pocketFactor), b2Vec2(ballRadius,tableSize.y+ballRadius*2*pocketFactor), b2Vec2(ballRadius*pocketFactor,tableSize.y), // up center
				b2Vec2(tableSize.x-ballRadius*pocketFactor*SQRT2,tableSize.y), b2Vec2(tableSize.x,tableSize.y+ballRadius*pocketFactor*SQRT2), b2Vec2(tableSize.x+ballRadius*pocketFactor*SQRT2,tableSize.y), b2Vec2(tableSize.x,tableSize.y-ballRadius*pocketFactor*SQRT2), // up right corner
				b2Vec2(tableSize.x,-tableSize.y+ballRadius*pocketFactor*SQRT2), b2Vec2(tableSize.x+ballRadius*pocketFactor*SQRT2,-tableSize.y), b2Vec2(tableSize.x,-tableSize.y-ballRadius*pocketFactor*SQRT2), b2Vec2(tableSize.x-ballRadius*pocketFactor*SQRT2,-tableSize.y), // down right corner
				b2Vec2(ballRadius*pocketFactor,-tableSize.y), b2Vec2(ballRadius,-tableSize.y-ballRadius*2*pocketFactor), b2Vec2(-ballRadius,-tableSize.y-ballRadius*2*pocketFactor), b2Vec2(-ballRadius*pocketFactor,-tableSize.y), // down center
				b2Vec2(-tableSize.x+ballRadius*pocketFactor*SQRT2,-tableSize.y), b2Vec2(-tableSize.x,-tableSize.y-ballRadius*pocketFactor*SQRT2), b2Vec2(-tableSize.x-ballRadius*pocketFactor*SQRT2,-tableSize.y), b2Vec2(-tableSize.x,-tableSize.y+ballRadius*pocketFactor*SQRT2) }; // down left corner
			
			b2ChainShape tableShape;
			tableShape.CreateLoop( vertices, sizeof(vertices)/sizeof(b2Vec2) );
			
			b2FixtureDef tableFixDef;
			tableFixDef.shape = &tableShape; 
			table->CreateFixture(&tableFixDef); //add a fixture to the body

			b2PolygonShape sensSh[6];
			sensSh[0].SetAsBox(ballRadius*2*pocketFactor,0.2f,b2Vec2(-tableSize.x,tableSize.y),toRadians(45));
			sensSh[1].SetAsBox(ballRadius*2*pocketFactor,0.2f,b2Vec2(0,tableSize.y+ballRadius*pocketFactor),toRadians(0));
			sensSh[2].SetAsBox(ballRadius*2*pocketFactor,0.2f,b2Vec2(tableSize.x,tableSize.y),toRadians(-45));
			sensSh[3].SetAsBox(ballRadius*2*pocketFactor,0.2f,b2Vec2(tableSize.x,-tableSize.y),toRadians(45));
			sensSh[4].SetAsBox(ballRadius*2*pocketFactor,0.2f,b2Vec2(0,-tableSize.y-ballRadius*pocketFactor),toRadians(0));
			sensSh[5].SetAsBox(ballRadius*2*pocketFactor,0.2f,b2Vec2(-tableSize.x,-tableSize.y),toRadians(-45));

			b2FixtureDef sensFix;
			sensFix.isSensor = true;
			for(int i=0; i<6; i++)
			{
				sensFix.shape = &sensSh[i];
				sensFix.userData = (void*)(i+20);
				sensors->CreateFixture(&sensFix);
			}
		} // Rectangle
		break;

	default:
		deleteGame();
		return false;
		break;
	} // switch(tableType)


	switch(gType)
	{
	case EightBall:
		{
			b2BodyDef bDef;
			bDef.type=b2_dynamicBody;
			bDef.position = whiteBallPos;
			b2Body* dBody = world->CreateBody(&bDef);
	
			b2CircleShape sh;
			sh.m_p.Set(0,0);
			sh.m_radius = ballRadius;//5/20.f;
	
			b2FixtureDef dbFix;
			dbFix.shape = &sh;
			dbFix.density = 1.f;
			dbFix.friction = 0.3f;
			dbFix.restitution = 0.70f; // white ball loses less energy ;)
			dbFix.userData = (void*)0; // ball no; 0=white
			dBody->CreateFixture(&dbFix);
			//dBody->SetGravityScale(0);
			//dBody->SetLinearDamping(dBody->GetMass());
			//dBody->SetAngularDamping(1.f);
	
			dbFix.restitution = 0.55f;

			int32 ballIndex[] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
			random_shuffle(ballIndex,ballIndex+15);
			swap(ballIndex[4],*find(ballIndex,ballIndex+15,8));

			for(int i=0;i<5;i++)
			{
				bDef.position.x=firstBallPos.x+i*ballRadius*SQRT3;
				bDef.position.y=firstBallPos.y+i*ballRadius;
				for(int j=0; j<=i; j++)
				{
					int32 index=(i+1)/2*i+j+1;
					b2Body* dBody = world->CreateBody(&bDef);
					dbFix.userData = (void*)ballIndex[index-1]; // ball no; 0=white
					dBody->CreateFixture(&dbFix);
					bDef.position.y-=2*ballRadius;
				}
			}
		}
		break;

	default:
		deleteGame();
		return false;
		break;
	} // switch(gType)

	return true;
}
    
bool BilliardPhysics::step() const
{
	if(!connected) return false;
	float32 hz = 60.f;
	int32 ballsCount = 0;
	for(b2Body* b = world->GetBodyList(); b; b = b->GetNext())
	{
		if(b->GetType() != b2_dynamicBody) continue;
		ballsCount++;
		b2Vec2 v = b->GetLinearVelocity();
		float vel = b2Sqrt(v.x*v.x+v.y*v.y);
		float force = 0;
		if(vel>1.f && vel<5.f) // v � (1;5)  -- ball diameter 0.5m
		{
			// negative value --- reverse vector return
			force = -b->GetMass()*vel*20/hz; // f=mv/t
		}
		else if(vel<0.1f) 
		{
			b->SetLinearVelocity(b2Vec2(0,0));
			b->SetAngularVelocity(0);
		}
		else // v � [0.1;1]
		{
			// negative value --- reverse vector return
			force = -b->GetMass()*vel*100/hz; // f=mv/t
		}

		if(force) // applying some force
		{
			float32 sinv = v.y/vel;
			float32 dy = sinv*force;
			float32 dx = b2Sqrt(force*force-dy*dy)*sgn(force*v.x);

			b->ApplyForceToCenter(b2Vec2(dx,dy));
		}
	}

	while(!destroyQueue.empty())
	{
		ballsCount--;
		//world->DestroyBody(*destroyQueue.begin()); // change object state? not delete?
		reinterpret_cast<ObjectBall*>((*destroyQueue.begin())->GetUserData())->changeState(ObjectBall::InPocket);
		destroyQueue.erase(destroyQueue.begin());
	}
	
	//run the default physics
	world->Step(sSet.timeStep, sSet.velocityIterations, sSet.positionIterations);

	return true;
}

bool BilliardPhysics::connect(std::vector<ObjectBall>& balls) const
{
	switch(actualGame)
	{
	case EightBall:
		{
			if(balls.size()!=16) return false;
			for(b2Body* b = world->GetBodyList(); b; b = b->GetNext())
			{
				if(b->GetType() != b2_dynamicBody) continue;
				uint ballNumber = reinterpret_cast<uint>(b->GetUserData());
				balls[ballNumber].connectBody(b);
				b->SetUserData(&balls[ballNumber]);
			}
		}
		break;
	}
	return true;
}

bool BilliardPhysics::hitBall(float force, float angle, int ballNumber) const
{
	if(!connected) return false;
	if(!force) return false;
	b2Body* bodyToHit=nullptr;
	for(b2Body* b = world->GetBodyList(); b; b = b->GetNext())
	{
		if(b->GetType() != b2_dynamicBody) continue;
		if(reinterpret_cast<ObjectBall*>((*destroyQueue.begin())->GetUserData())->ballNumber==ballNumber)
		{
			bodyToHit=b;
			break;
		}
	}
	if(!bodyToHit || bodyToHit->GetFixtureList()->IsSensor()) return false;

	// applying force
	float32 dy = sin(angle*DEGTORAD)*force;
	float32 dx = cos(angle*DEGTORAD)*force;

	bodyToHit->ApplyForceToCenter(b2Vec2(dx,dy));
	
	return true;
}

bool BilliardPhysics::testNewPos(float x, float y, int ballNumber) const
{
	if(!connected) return false;
	if(!actualSurface.testPoint(x,y)) return false;
	for(b2Body* b = world->GetBodyList(); b; b = b->GetNext())
	{
		if(b->GetType() != b2_dynamicBody) continue;
		if(reinterpret_cast<ObjectBall*>((*destroyQueue.begin())->GetUserData())->ballNumber==ballNumber)
			continue;

		b2Vec2 bPos = b->GetPosition();
		b2Vec2 diff(bPos.x-x,bPos.y-y);
		if(b2Sqrt(diff.x*diff.x+diff.y*diff.y)<ballRadius) return false;
	}

	return true;
}

bool BilliardPhysics::moveBall(float x, float y, int ballNumber) const
{
	if(!connected) return false;
	if(!testNewPos(x,y,ballNumber)) return false;
	bool found=false;

	for(b2Body* b = world->GetBodyList(); b; b = b->GetNext())
	{
		if(b->GetType() != b2_dynamicBody) continue;
		if(reinterpret_cast<ObjectBall*>((*destroyQueue.begin())->GetUserData())->ballNumber==ballNumber)
		{
			b->SetTransform(b2Vec2(x,y),b->GetAngle());
			if(reinterpret_cast<ObjectBall*>((*destroyQueue.begin())->GetUserData())->getState() == ObjectBall::InPocket)
				reinterpret_cast<ObjectBall*>((*destroyQueue.begin())->GetUserData())->changeState(ObjectBall::Normal);
			break;
		}
	}
	if(!found) return false;

	return true;
}

bool BilliardPhysics::pushBall(int ballNumber, float step) const
{
	if(!connected) return false;
	b2Body* body=nullptr;
	for(b2Body* b = world->GetBodyList(); b; b = b->GetNext())
	{
		if(b->GetType() != b2_dynamicBody) continue;
		if(reinterpret_cast<ObjectBall*>((*destroyQueue.begin())->GetUserData())->ballNumber==ballNumber)
		{
			body=b;
			break;
		}
	}
	if(!body) return false;

	// find new position
	int max=static_cast<int>(5/step); // max square "radius"
	int actMax=0;
	enum direction {UpRight, DownRight, DownLeft, UpLeft} dir=DownRight;
	bool found=false;
	for(int x=0,y=0; actMax<=max;)
	{
		if(testNewPos(x*step,y*step,ballNumber))
		{
			found=true;
			moveBall(x*step,y*step,ballNumber);
			break;
		}

		switch(dir)
		{
		case UpRight:
			if(y==actMax)
			{
				dir=DownRight;
				x++; y--;
			}
			else { x++; y++; }
			break;

		case DownRight:
			if(x==actMax)
			{
				dir=DownLeft;
				x++; actMax++;
			}
			else { x++; y--; }
			break;
			
		case DownLeft:
			if(y==-actMax)
			{
				dir=UpLeft;
				x--; y++;
			}
			else { x--; y--; }
			break;

		case UpLeft:
			if(x==-actMax)
			{
				dir=UpRight;
				x++; y++;
			}
			else { x--; y++; }
			break;
		}
	}

	return found;
}


const std::vector<int>& BilliardPhysics::getLastStuckedBalls() const
{
	lastStucked.clear();
	if(!connected) return lastStucked;
	for(b2Body* it : destroyQueue)
	{
		lastStucked.push_back(reinterpret_cast<ObjectBall*>((*destroyQueue.begin())->GetUserData())->ballNumber);
	}

	return lastStucked;
}


BilliardPhysics::State BilliardPhysics::getState() const
{
	if(!connected) return State::GameDoesntExist;
	for(b2Body* b = world->GetBodyList(); b; b = b->GetNext())
	{
		if(b->GetType() != b2_dynamicBody) continue;
		if(!(b->GetLinearVelocity() == b2Vec2(0,0))) return State::BallsAreMoving; 
	}

	return State::Idle;
}

void BilliardPhysics::deleteGame()
{
	if(world) delete world;
	world=nullptr;
	sensors=nullptr; // world deletes it
	connected=false;
}

BilliardPhysics::~BilliardPhysics()
{
	deleteGame();
}