# add source file from this directory
set(CM_BILLIARD_SRC ${CM_BILLIARD_SRC}
    ${CMAKE_CURRENT_SOURCE_DIR}/BilliardPhysics.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/BilliardPhysics.h
    ${CMAKE_CURRENT_SOURCE_DIR}/Surface.h
    PARENT_SCOPE
    )