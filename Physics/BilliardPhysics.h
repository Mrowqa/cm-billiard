#ifndef BILLIARDPHYSICS_H
#define BILLIARDPHYSICS_H

#include <Box2D/Box2D.h>
#include <vector>
#include "../Objects/ObjectBall.h"
#include "Surface.h"

//main collision call back function
class PocketContactListener : public b2ContactListener
{
	void BeginContact(b2Contact* contact);
};


class BilliardPhysics
{
public:
	enum PoolTable {Rectangle, Hexagon};
	enum GameType {EightBall, NineBall, Snooker};
	enum State {Idle, BallsAreMoving, GameDoesntExist};

private:
	struct simulateSettings
	{
		float32 timeStep;
		int32 velocityIterations;
		int32 positionIterations;
		
		simulateSettings(float32 _timeStep=1.f/60.f, int32 _velocityIterations=8, int32 _positionIterations=3)
			: timeStep(_timeStep), velocityIterations(_velocityIterations), positionIterations(_positionIterations) {}
	} sSet;

	b2Body* sensors;
	PocketContactListener myPCListener;
	b2World* world;
	PoolTable actualTable;
	GameType actualGame;
	Surface actualSurface;
	bool connected;

	float32 ballRadius;
	mutable std::vector<int> lastStucked;

public:
	BilliardPhysics() : connected(false), world(nullptr), sensors(nullptr) {}
    BilliardPhysics(PoolTable tableType, GameType gType);
	~BilliardPhysics();
    
	bool step() const;
	bool connect(std::vector<ObjectBall>& balls) const;
	bool hitBall(float force, float angle=0.f /*in degrees*/, int ballNumber=0) const;
	bool testNewPos(float x, float y, int ballNumber=0) const;
	bool moveBall(float x, float y, int ballNumber=0) const;
	bool pushBall(int ballNumber=0, float step=0.1f) const;
	bool createNewGame(PoolTable tableType=Rectangle, GameType gType=EightBall);
	bool isGameExist() const {return world?true:false;} // warnings... -_-'
	void deleteGame();

	const std::vector<int>& getLastStuckedBalls() const; // returns reference for performance
	State getState() const;
	// methods: lineAfterBallHit? changeBallState? ...
};


#endif //BILLIARDPHYSICS_H