#include <iostream>
#include <sstream>
#include "CMBilliardConfig.h"
#include <SFML/Graphics.hpp>
using namespace std;

inline sf::Color getColorFromTime(float time,int factor)
{
	return sf::Color(sf::Uint8((std::sin(time*factor*1)+1)*128),
			sf::Uint8((std::cos(time*factor*5)+1)*128),
			sf::Uint8((std::sin(time*factor*10)+1)*128));
}

// This is temporary code -- that's why it's so awful.

int main()
{
	ostringstream info;
	info << "Source code in development\n"
			"Actual version: " << CM_BILLIARD_VERSION_MAJOR << "." << CM_BILLIARD_VERSION_MINOR << "." << CM_BILLIARD_VERSION_EXTRA_INFO << "\n";

	cout << info.str();


	sf::RenderWindow window(sf::VideoMode(600,450),"Cutie Marks Billiard - Prerelease build");
	sf::Vector2u wndSize=window.getSize();
	sf::Vector2f circlePos(wndSize.x/2.f,wndSize.y/2.f);
	sf::Vector2f circleVel(0,0);
	window.setFramerateLimit(60);
	sf::Clock stopwatch;

	sf::String str(info.str());
	sf::Font font;
	font.loadFromFile(CM_BILLIARD_DATA_DIR"fonts/DISTGRG_.ttf");
	sf::Text infoTxt(str,font,20);
	infoTxt.setPosition(5,wndSize.y-infoTxt.getLocalBounds().height-10);

	while(window.isOpen())
	{
		sf::Event event;
		while(window.pollEvent(event))
		{
			if(event.type==sf::Event::Closed)
				window.close();
		} //while

		register float accelerationDivider=1000;
		circleVel+=sf::Vector2f((window.mapPixelToCoords(sf::Mouse::getPosition(window)).x-circlePos.x)/accelerationDivider,(window.mapPixelToCoords(sf::Mouse::getPosition(window)).y-circlePos.y)/accelerationDivider);
		if(sf::Mouse::isButtonPressed(sf::Mouse::Left)) circleVel*=1.05f;
		circlePos+=circleVel;
		float circleSpeed = sqrt(circleVel.x*circleVel.x+circleVel.y*circleVel.y);
		if(circleSpeed>5)
		{
			circleVel.x/=1.05f;
			circleVel.y/=1.05f;
			circleSpeed = sqrt(circleVel.x*circleVel.x+circleVel.y*circleVel.y);
		}

		if(circlePos.x>wndSize.x)
		{
			circlePos.x=static_cast<float>(wndSize.x);
			if(circleVel.x>0) circleVel.x*=-1;
		}
		else if(circlePos.x<0)
		{
			circlePos.x=0;
			if(circleVel.x<0) circleVel.x*=-1;
		}
		else if(circlePos.y>wndSize.y)
		{
			circlePos.y=static_cast<float>(wndSize.y);
			if(circleVel.y>0) circleVel.y*=-1;
		}
		else if(circlePos.y<0)
		{
			circlePos.y=0;
			if(circleVel.y<0) circleVel.y*=-1;
		}

		window.clear();

		sf::CircleShape circle(((std::sin(stopwatch.getElapsedTime().asSeconds()))*0+0.4f-circleSpeed/5)*wndSize.y/8+wndSize.y/4);
		circle.setOrigin(sf::Vector2f(circle.getRadius(),circle.getRadius()));
		circle.setPosition(circlePos);
		circle.setFillColor(getColorFromTime(stopwatch.getElapsedTime().asSeconds(),1));
		window.draw(circle);
		
		circle.setFillColor(getColorFromTime(stopwatch.getElapsedTime().asSeconds(),-3));
		circle.setRadius(10);
		circle.setOrigin(sf::Vector2f(circle.getRadius(),circle.getRadius()));
		circle.setPosition(window.mapPixelToCoords(sf::Mouse::getPosition(window)).x,window.mapPixelToCoords(sf::Mouse::getPosition(window)).y);
		window.draw(circle);

		window.draw(infoTxt);

		window.display();
	} //while
	
	return 0;
}
