#ifndef APPSTATEMANAGER_H
#define APPSTATEMANAGER_H

#include "AppState.h"

class AppStateManager
	: public AppState
{
public:
	AppStateFactory* defaultState;
	Render* defaultRender;

	AppStateManager(AppStateFactory* defaultStateFactory=nullptr,Render* render=nullptr) : AppState(0,render), defaultState(defaultStateFactory), defaultRender(render) {}
	virtual ~AppStateManager(void) {}
	
	static bool start(AppStateFactory* stateFactory, Render* render=nullptr, void* userData=nullptr);
	bool start(void* userData=nullptr) const		{return start(defaultState,defaultRender,userData);}
};

#endif // APPSTATEMANAGER_H