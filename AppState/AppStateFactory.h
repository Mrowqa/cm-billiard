#ifndef APPSTATEFACTORY_H
#define APPSTATEFACTORY_H

#include "../Render/Render.h"

typedef unsigned int uint;
class AppState;

class AppStateFactory
{
public:
	virtual AppState* create(uint level=0, Render* render=nullptr) const=0;
};

template<typename InheritedState>
class AppStateFactoryCustom
	: public AppStateFactory
{
public:
	virtual AppState* create(uint level=0, Render* render=nullptr) const {return new InheritedState(level,render);}
};

#endif // APPSTATEFACTORY_H