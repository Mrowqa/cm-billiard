#include "AppState.h"


bool AppState::changeState(const ChangeStateQuery& csq)
{
	switch(csq.csType)
	{
	case ChangeStateType::ExitThisLevel:
	case ChangeStateType::ExitAllLevels:
		break;

	case ChangeStateType::GoToLevel:
		if(csq.newLevel >= actualLevel) return false;
		break;
		
	case ChangeStateType::CreateLevelFromHere:
	case ChangeStateType::CreateLevelFromBase:
		if(!csq.factoryStateHandle) return false;
		break;
	
	case ChangeStateType::CreateLevelFromCustomLevel:
		if(!csq.factoryStateHandle || csq.newLevel >= actualLevel) return false;
		break;

	default:
		return false;
		break;
	}

	csQuery=csq;
	wcsQuery=WaitingCSQuery::FromThisLevel;
	return true;
}

AppState::ChangeStateQuery AppState::stateManagerLoop(void* data)
{
	AppState* higherLevel=nullptr;
	userData=data;
	init();

	while(true)
	{
		while(wcsQuery!=WaitingCSQuery::Not)
		{
			bool newQuery=false;

			switch(csQuery.csType) // first query, from THIS level
			{
			case ChangeStateType::ExitThisLevel:
				if(wcsQuery==WaitingCSQuery::FromThisLevel) return csQuery;
				break;

			case ChangeStateType::ExitAllLevels:
				return csQuery;
				break;

			case ChangeStateType::GoToLevel:
				if(actualLevel!=csQuery.newLevel) return csQuery;
				break;

			case ChangeStateType::CreateLevelFromCustomLevel:
				if(actualLevel!=csQuery.newLevel) return csQuery;
				wcsQuery=WaitingCSQuery::FromThisLevel;
				newQuery=true;
				break;
			
			case ChangeStateType::CreateLevelFromBase:
				if(actualLevel!=0) return csQuery;
				wcsQuery=WaitingCSQuery::FromThisLevel;
				newQuery=true;
				break;

			case ChangeStateType::CreateLevelFromHere:
				higherLevel=csQuery.factoryStateHandle->create(actualLevel+1,render);
				csQuery=higherLevel->stateManagerLoop(csQuery.userData);
				delete higherLevel;
				higherLevel=nullptr;
				wcsQuery=WaitingCSQuery::FromHigherLevel;
				newQuery=true;
				break;
			}

			if(!newQuery) break;
		}

		loop();
	}
}