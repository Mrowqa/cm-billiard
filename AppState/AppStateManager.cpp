#include "AppStateManager.h"


bool AppStateManager::start(AppStateFactory* stateFactory, Render* render, void* userData)
{
	if(!stateFactory) return false;

	AppStateManager asManager(stateFactory,render);
	ChangeStateQuery csq={CreateLevelFromHere,userData,stateFactory,render};
	if(!asManager.changeState(csq)) return false;
	asManager.stateManagerLoop();
	return true;
}