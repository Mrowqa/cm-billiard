#ifndef APPSTATE_H
#define APPSTATE_H

#include "AppStateFactory.h"
#include "../Render/Render.h"

typedef unsigned int uint;


class AppState
{
protected:
	enum ChangeStateType {ExitThisLevel, ExitAllLevels, GoToLevel, CreateLevelFromHere, CreateLevelFromBase, CreateLevelFromCustomLevel};
	struct ChangeStateQuery
	{
		ChangeStateType csType;
		void* userData;
		AppStateFactory* factoryStateHandle;
		Render* render;
		uint newLevel;
	};

	void* userData;

	AppState(uint myLevel, Render* _render) : actualLevel(myLevel), wcsQuery(WaitingCSQuery::Not), render(_render) {}
	bool changeState(const ChangeStateQuery& csQuery);
	bool isAwaitingCSQuery() const	{return wcsQuery!=WaitingCSQuery::Not;}
	ChangeStateQuery stateManagerLoop(void* data=nullptr);
	virtual void init() {}
	virtual void loop() {ChangeStateQuery csq={ExitThisLevel}; changeState(csq);} // when this method won't be overloaded then function will get looped - we don't want this, aren't we? :)

private:
	enum WaitingCSQuery {Not, FromThisLevel, FromHigherLevel} wcsQuery;
	ChangeStateQuery csQuery;
	const uint actualLevel;
	Render* const render;

public:
	AppState() : actualLevel(0), wcsQuery(WaitingCSQuery::Not), render(nullptr) {}
	virtual ~AppState(void) {}

	uint getActualLevel() const	{return actualLevel;} // maybe protected?
	Render* getRender() const	{return render;}

	//void begin(void* data=nullptr) {stateManagerLoop(data);}
};

#endif // APPSTATE_H